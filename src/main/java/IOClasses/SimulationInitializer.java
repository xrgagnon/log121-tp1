package IOClasses;

import ComponentClasses.*;
import ComponentClasses.SimulatedComponent.*;
import DTOClasses.Connector;
import DTOClasses.FactoryConfig;
import DTOClasses.FactoryConfig.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class SimulationInitializer {

    private final String configPath = "src/ressources/configuration.xml";

    private ArrayList<FactoryConfig> factoryConfigs;
    private Document config;

    public SimulationInitializer(){
        config = loadDocument();
        factoryConfigs = new ArrayList<>();

        loadConfigs();


    }

    public void loadConfigs(){
        NodeList metadata = config.getElementsByTagName("metadonnees").item(0).getChildNodes();

        iterable(metadata).forEach(this::readFactoryTypeConfig);
    }


    public ArrayList<Simulated> getSimulationObjects(){

        ArrayList<Simulated> simulationObjects = new ArrayList<>();
        ArrayList<SimulatedLocation> locations = new ArrayList<>();
        ArrayList<SimulatedLink> links = new ArrayList<>();

        NodeList simulationSetupNodeList = config.getElementsByTagName("simulation").item(0).getChildNodes();
        iterable(simulationSetupNodeList).forEach(simulationNode -> {
            try {
                if (simulationNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element simulationElement = (Element) simulationNode;

                    if (simulationElement.getTagName().equals("usine")){
                        FactoryType type = stringTagToFactoryType(simulationElement.getAttribute("type"));
                        int xPosition = Integer.parseInt(simulationElement.getAttribute("x"));
                        int yPosition = Integer.parseInt(simulationElement.getAttribute("y"));
                        int id = Integer.parseInt(simulationElement.getAttribute("id"));

                        FactoryConfig factoryConfig = getConfigForType(type);

                        //TODO move image loading to somewhere else
                        BufferedImage[] stateIcons = new BufferedImage[4];
                        IconIO imageFinder = new IconIO();
                        for( int i = 0; i < factoryConfig.getImagePaths().length; i++){
                            stateIcons[i] = imageFinder.loadImage(factoryConfig.getImagePaths()[i]);
                        }

                        SimulatedLocation newFactory;
                        if (type == FactoryType.DEPOT){
                            newFactory = new SimulatedDepot(xPosition,yPosition,stateIcons,id, factoryConfig, type);
                        }
                        else {
                            newFactory = new SimulatedFactory(xPosition,yPosition,stateIcons,id, factoryConfig, type);
                        }


                        locations.add(newFactory);
                    }
                    else if (simulationElement.getTagName().equals("chemins")){
                        //TODO execute after loading factories
                        NodeList roadNodes = simulationElement.getChildNodes();
                        iterable(roadNodes).forEach(roadNode -> {
                            if (roadNode.getNodeType() == Node.ELEMENT_NODE){
                                Element roadElement = (Element) roadNode;
                                int firstFactoryId = Integer.parseInt(roadElement.getAttribute("de"));
                                int secondFactoryId = Integer.parseInt(roadElement.getAttribute("vers"));

                                SimulatedLink newLink = new SimulatedLink();

                                final ComponentType[] connectorComponent = new ComponentType[1];
                                locations.forEach(location -> {
                                    if (location.getId() == firstFactoryId){
                                        connectorComponent[0] = location.getConfig().getExitConnectors().get(0).getComponentType();
                                    }
                                });

                                locations.forEach(location -> {
                                    try {

                                    if (location.getId() == firstFactoryId) {
                                        FactoryConnector newConnector = new FactoryConnector(location.getConfig().getExitConnectors().get(0), location, newLink);
                                        newLink.setFirstFactoryConnector(newConnector);
                                        location.addExitConnector(newConnector);
                                    } else if (location.getId() == secondFactoryId) {

                                        final FactoryConnector[] newConnector = new FactoryConnector[1];

                                        location.getConfig().getEntryConnectors().forEach(connector -> {
                                            if (connector.getComponentType() == connectorComponent[0]) {
                                                newConnector[0] = new FactoryConnector(connector, location, newLink);
                                            }
                                        });

                                        location.addEntryConnector(newConnector[0]);
                                        newLink.setSecondFactoryConnector(newConnector[0]);
                                    }
                                    }
                                    catch (Exception e){
                                        System.out.println("ERROR"+e.getMessage());
                                    }
                                });




                                links.add(newLink);

                            }
                        });



                        simulationObjects.addAll(locations);
                        simulationObjects.addAll(links);

                        SimulatedDepot mainDepot = getMainDepot(simulationObjects);
                        simulationObjects.forEach(simulated -> {
                            if (simulated.getClass() == SimulatedFactory.class){
                                mainDepot.addListener((SimulatedFactory)simulated);
                            }
                        });

                    }
                    else {
                        System.out.println("Unknown simulation object: " + simulationElement.getTagName());
                    }

                }

            } catch (Exception e) {
                System.out.println("Error "+e.getMessage());
            }
        });
        return simulationObjects;


    }
    
    private void readFactoryTypeConfig(Node node){
        try {
            FactoryType type;
            ArrayList<Connector> entryComponents = new ArrayList<>();
            ArrayList<Connector> exitComponents = new ArrayList<>();
            String[] imagePaths = new String[4];
            int productionInterval = -1;

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element nodeElement = (Element) node;
                type = stringTagToFactoryType(nodeElement.getAttribute("type"));

                if (type.equals(FactoryType.DEPOT)){
                    NodeList entryConfigList = nodeElement.getElementsByTagName("entree");
                    iterable(entryConfigList).forEach(entryConfig -> {
                        try {
                            if (entryConfig.getNodeType() == Node.ELEMENT_NODE) {
                                Element entryElement = (Element) entryConfig;
                                int capacity = Integer.parseInt(entryElement.getAttribute("capacite"));
                                ComponentType componentType = stringTagToComponentType(entryElement.getAttribute("type"));
                                Connector entryConnector = new Connector(EntryOrExit.ENTRY, componentType, capacity);
                                entryComponents.add(entryConnector);
                            }
                        } catch (Exception e) {
                            System.out.println("ERROR creating depot entry connector: "+e.getMessage());
                        }
                    });
                }
                else {
                    NodeList entryConfigList = nodeElement.getElementsByTagName("entree");
                    iterable(entryConfigList).forEach(entryConfig -> {
                        try {
                            if (entryConfig.getNodeType() == Node.ELEMENT_NODE) {
                                Element entryElement = (Element) entryConfig;
                                int capacity = Integer.parseInt(entryElement.getAttribute("quantite"));
                                ComponentType componentType = stringTagToComponentType(entryElement.getAttribute("type"));
                                Connector entryConnector = new Connector(EntryOrExit.ENTRY, componentType, capacity);
                                entryComponents.add(entryConnector);
                            }
                        } catch (Exception e) {
                            System.out.println("ERROR creating depot entry connector: "+e.getMessage());
                        }
                    });
                }



                NodeList exitConfigList = nodeElement.getElementsByTagName("sortie");
                iterable(exitConfigList).forEach(exitConfig -> {
                    try {
                        if (exitConfig.getNodeType() == Node.ELEMENT_NODE) {
                            Element exitElement = (Element) exitConfig;
                            ComponentType componentType = stringTagToComponentType(exitElement.getAttribute("type"));
                            Connector exitConnector = new Connector(EntryOrExit.EXIT, componentType, -1);
                            exitComponents.add(exitConnector);
                        }
                    } catch (Exception e) {
                        System.out.println("ERROR creating  exit connector: "+e.getMessage());
                    }
                });

                NodeList imagePathsNodes = nodeElement.getElementsByTagName("icones").item(0).getChildNodes();
                iterable(imagePathsNodes).forEach(iconNode -> {
                    try {
                        if (iconNode.getNodeType() == Node.ELEMENT_NODE) {
                            Element iconElement = (Element) iconNode;
                            String representedState = iconElement.getAttribute("type");
                            String path = iconElement.getAttribute("path");
                            imagePaths[stringTypeToIndex(representedState)] = path;
                        }

                    } catch (Exception e) {
                        System.out.println("ERROR getting icons: "+e.getMessage());
                    }
                });

                try {
                    NodeList list =  nodeElement.getElementsByTagName("interval-production");
                    if (list.getLength() > 0){
                        Element productionNode = (Element)list.item(0);
                        productionInterval = Integer.parseInt(productionNode.getTextContent());
                    }
                }
                catch(Exception e){
                    System.out.println("ERROR parsing production interval: "+e.getMessage());
                }


                FactoryConfig newConfig = new FactoryConfig(type, imagePaths, productionInterval, entryComponents, exitComponents);
                factoryConfigs.add(newConfig);
            }}
        catch(Exception e){
            System.out.println("ERROR initializing factory config: "+e.getMessage());
        }
    }







    private SimulatedDepot getMainDepot(ArrayList<Simulated> simulationObjects ){
        final SimulatedDepot[] depot = new SimulatedDepot[1];
        simulationObjects.forEach(simulated -> {
            if (simulated.getClass() == SimulatedDepot.class){
                depot[0] = (SimulatedDepot) simulated;
            }
        });
        return depot[0];
    }


    private Document loadDocument() {
        try {

            File fXmlFile = new File(configPath);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            doc.getDocumentElement().normalize();
            return doc;
        }
        catch (Exception e){
            System.out.println(e.getMessage());
            throw new RuntimeException("Failed to load configs");
        }
    }


    //Util method to iterate on nodelist
    public static Iterable<Node> iterable(final NodeList nodeList) {
        return () -> new Iterator<Node>() {

            private int index = 0;

            @Override
            public boolean hasNext() {
                return index < nodeList.getLength();
            }

            @Override
            public Node next() {
                if (!hasNext())
                    throw new NoSuchElementException();
                return nodeList.item(index++);
            }
        };
    }

    private FactoryType stringTagToFactoryType(String tag) throws Exception{
        FactoryType type = null;

        switch(tag){
            case "usine-matiere":
                type = FactoryType.MATTER_FACTORY;
                break;
            case "usine-aile":
                type = FactoryType.WING_FACTORY;
                break;
            case "usine-moteur":
                type=FactoryType.ENGINE_FACTORY;
                break;
            case "usine-assemblage":
                type=FactoryType.ASSEMBLY_FACTORY;
                break;
            case "entrepot":
                type=FactoryType.DEPOT;
                break;
            default:
                throw new Exception("Unknown or null tag: " + tag);
        }

        return type;
    }

    private ComponentType stringTagToComponentType(String tag) throws Exception{
        ComponentType type = null;

        switch(tag){
            case "metal":
                type = ComponentType.METAL;
                break;
            case "aile":
                type = ComponentType.WING;
                break;
            case "moteur":
                type=ComponentType.ENGINE;
                break;
            case "avion":
                type = ComponentType.PLANE;
                break;
            default:
                throw new Exception("Unknown or null component type: " + tag);
        }

        return type;
    }

    private int stringTypeToIndex(String tag) throws Exception{
        int index = 0;

        switch(tag){
            case "vide":
                index = 0;
                break;
            case "un-tiers":
                index = 1;
                break;
            case "deux-tiers":
                index = 2;
                break;
            case "plein":
                index = 3;
                break;
            default:
                throw new Exception("Unknown or null component type: " + tag);
        }

        return index;
    }

    private FactoryConfig getConfigForType(FactoryType type){

        final FactoryConfig[] config = new FactoryConfig[1];

        factoryConfigs.forEach(possibleConfig -> {
            if (type == possibleConfig.getType()){
                config[0] = possibleConfig;

            }
        });

        return config[0];
    }

    private int[] getPositionsFromFactory(int id, ArrayList<SimulatedLocation> simulatedLocations){
        int[] position = new int[2];

        simulatedLocations.forEach(location -> {
            if (id == location.getId()){
                position[0] = location.getxPosition();
                position[1] = location.getyPosition();
            }
        });
        return position;
    }

}
