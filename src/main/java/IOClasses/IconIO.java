package IOClasses;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class IconIO {



    public BufferedImage loadImage(String filePath) {
        BufferedImage loadedImage;
        try {
            loadedImage = ImageIO.read(new File(filePath));
        }
        catch(Exception e){
            System.out.println(e.getMessage());
            loadedImage = null;
        }
        return loadedImage;

    }
}
