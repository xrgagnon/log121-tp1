package ComponentClasses;

import ComponentClasses.SaleBehavior.SaleStragegyInverval;
import ComponentClasses.SaleBehavior.SaleStrategy;
import ComponentClasses.SaleBehavior.SaleStrategyRandom;

public class SimulationStatistics {

    private static int numberOfPlanesProduced = 0;
    private static int numberOfPlanesSold = 0;
    private static String currentSaleStrategy = "Random Sale Strategy";
    private static boolean strategyHasChanged = false;

    public static void notifyNewComponentProduced(SimulatedComponent component){
        if (component.getType().name().equals("PLANE"))
            numberOfPlanesProduced++;
    }



    public static void planeSold(){
        numberOfPlanesSold++;
    }

    public static int getNumberOfPlanesProduced() {
        return numberOfPlanesProduced;
    }

    public static int getNumberOfPlanesSold() {
        return numberOfPlanesSold;
    }

    public static SaleStrategy getCurrentSaleStrategy() {
        SaleStrategy saleStrategy;

        if (currentSaleStrategy.equals("Random Sale Strategy")){
            return new SaleStrategyRandom(100);
        }
        else {
            return new SaleStragegyInverval();
        }

    }

    public static void setCurrentSaleStrategy(String currentSaleStrategy) {
        strategyHasChanged = true;
        SimulationStatistics.currentSaleStrategy = currentSaleStrategy;
    }

    public static void saleStrategyUpdateConfirmed(){
        strategyHasChanged = false;
    }

    public static boolean hasStrategyChanged(){
        return strategyHasChanged;
    }
}
