package ComponentClasses;

import java.awt.*;
import java.awt.image.BufferedImage;



public class SimulatedComponent extends SimulatedIcon {

    public enum ComponentType {METAL,ENGINE,WING,PLANE}
    private ComponentType type;

    public SimulatedComponent(int x, int y, BufferedImage i, ComponentType t) {
        super(x, y, i);
        type = t;
    }

    public SimulatedComponent(BufferedImage i, ComponentType t) {
        super(0, 0, i);
        type = t;
    }

    public ComponentType getType() {
        return type;
    }

    public void setType(ComponentType type) {
        this.type = type;
    }

    @Override
    public void Draw(Graphics g) {


        if (super.isDisplayed())
        g.drawImage(super.getDefaultImage(),super.getxPosition(),super.getyPosition(),this);
    }

    @Override
    public void Simulate(){
        //super.addToPosition(1,1);
    }

    public void move(int[] movement){
        if (movement.length == 2){
            super.setxPosition(super.getxPosition() + movement[0]);
            super.setyPosition(super.getyPosition() + movement[1]);
        }
    }





}
