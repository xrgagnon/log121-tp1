package ComponentClasses;

import DTOClasses.FactoryConfig;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SimulatedLocation extends SimulatedIcon {

    private BufferedImage[] stateImages;
    private int id;
    private final ArrayList<FactoryConnector> entryConnectors;
    private final ArrayList<FactoryConnector> exitConnectors;
    private FactoryConfig config;
    private FactoryConfig.FactoryType type;
    private double productionFactor = 1;

    private int  actualProductionDelay = 0;


    private double productionProgress = 0;

    private BufferedImage currentIcon;


    public SimulatedLocation(int x, int y, BufferedImage[] si, int id, FactoryConfig c, FactoryConfig.FactoryType type) {
        super(x, y, si[0]);
        stateImages = si;
        this.id = id;
        config = c;
        this.type = type;
        entryConnectors = new ArrayList<>();
        exitConnectors = new ArrayList<>();

        currentIcon = super.getDefaultImage();

        actualProductionDelay = config.getProductionInterval();

        setVisible(false);
    }



    @Override
    public void Simulate(){

        productionProgress();

        calculateOutput();

        updateIcon();
    }



    private void calculateOutput(){
        if (productionProgress >= actualProductionDelay && actualProductionDelay > 0){
            outputComponents();
            productionProgress = 0;
        }
    }

    private void outputComponents(){

        exitConnectors.forEach(connector -> {
            SimulatedComponent exitComponent = ComponentManager.createComponent(connector.getComponentType());
            exitComponent.setDisplayed(false);
            connector.insertComponent(exitComponent);
        });


    }

    private void productionProgress(){

        Map<SimulatedComponent.ComponentType, BigInteger> requirements = new HashMap<>();
        Map<SimulatedComponent.ComponentType, BigInteger> numberOfComponentsAvailable = new HashMap<>();

        determineRequirementsAndAvailableComponents(requirements,numberOfComponentsAvailable);

        boolean hasRequirements = areRequirementsFulfilled(requirements,numberOfComponentsAvailable);

        executeProduction(hasRequirements, numberOfComponentsAvailable);

    }

    private void determineRequirementsAndAvailableComponents(Map<SimulatedComponent.ComponentType, BigInteger> requirements, Map<SimulatedComponent.ComponentType, BigInteger> numberOfComponentsAvailable){
        entryConnectors.forEach(factoryConnector -> {
            SimulatedComponent.ComponentType type = factoryConnector.getComponentType();
            if (!requirements.containsKey(type)){
                requirements.put(type,new BigInteger(""+factoryConnector.getComponentsRequired()));
                numberOfComponentsAvailable.put(type,new BigInteger(""+factoryConnector.getNumberOfHeldComponents()));
            } else if (requirements.containsKey(type)){

                numberOfComponentsAvailable.computeIfPresent(type,(key,val) -> val.add(new BigInteger(""+factoryConnector.getNumberOfHeldComponents())));

            }
        });
    }

    private void executeProduction(boolean hasRequirements, Map<SimulatedComponent.ComponentType, BigInteger> numberOfComponentsAvailable){
        if (actualProductionDelay > 0 && hasRequirements){

            productionProgress += 1 * productionFactor;
            beginProduction(numberOfComponentsAvailable);
        }
        else if (productionProgress > 0){
            productionProgress++;
        }
    }

    private boolean areRequirementsFulfilled(Map<SimulatedComponent.ComponentType, BigInteger> requirements, Map<SimulatedComponent.ComponentType, BigInteger> numberOfComponentsAvailable){
        final boolean[] hasRequirements = {true};
        if (!requirements.isEmpty() && numberOfComponentsAvailable.isEmpty())
            hasRequirements[0] = false;

        for(Map.Entry<SimulatedComponent.ComponentType,BigInteger> requirement : requirements.entrySet()){
            if (numberOfComponentsAvailable.get(requirement.getKey()) != null && numberOfComponentsAvailable.get(requirement.getKey()).intValue() < requirement.getValue().intValue() ){
                hasRequirements[0] = false;
            }
        }
        return hasRequirements[0];
    }

    private void beginProduction(Map<SimulatedComponent.ComponentType, BigInteger> numberOfComponentsToConsume){
        ArrayList<SimulatedComponent> consumedComponents = consumeComponents(numberOfComponentsToConsume);

        ComponentManager.removeComponents(consumedComponents);
    }

    private ArrayList<SimulatedComponent> consumeComponents(Map<SimulatedComponent.ComponentType, BigInteger> numberOfComponentsToConsume){

        ArrayList<SimulatedComponent> consumedComponents = new ArrayList<>();
        entryConnectors.forEach(factoryConnector -> {


            if (numberOfComponentsToConsume.get(factoryConnector.getComponentType()) != null &&
                    numberOfComponentsToConsume.get(factoryConnector.getComponentType()).intValue() > 0){


                consumedComponents.add(factoryConnector.extractComponent());
                numberOfComponentsToConsume.computeIfPresent(factoryConnector.getComponentType(),(key,val) ->
                        val.subtract(new BigInteger("1")));
            }
        });
        return consumedComponents;
    }

    private void updateIcon(){

        double productionPercentage = (double)productionProgress/(double)actualProductionDelay;

        if (productionPercentage < 0.25){
            currentIcon = stateImages[0];
        }
        else if (productionPercentage < 0.50){
            currentIcon = stateImages[1];
        }
        else if (productionPercentage < 0.75){
            currentIcon = stateImages[2];
        }
        else if (productionPercentage >= 0.75) {
            currentIcon = stateImages[3];
        }
        else {
            currentIcon = stateImages[0];
        }

    }


    @Override
    public void Draw(Graphics g) {
        if (super.isDisplayed())
        g.drawImage(currentIcon,super.getxPosition(),super.getyPosition(),this);
    }



    public BufferedImage[] getStateImages() {
        return stateImages;
    }

    public void setStateImages(BufferedImage[] stateImages) {
        this.stateImages = stateImages;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public FactoryConfig getConfig() {
        return config;
    }

    public void setConfig(FactoryConfig config) {
        this.config = config;
    }

    public FactoryConfig.FactoryType getType() {
        return type;
    }

    public void setType(FactoryConfig.FactoryType type) {
        this.type = type;
    }

    public ArrayList<FactoryConnector> getEntryConnectors() {
        return entryConnectors;
    }

    public void addEntryConnector(FactoryConnector connector) {
        this.entryConnectors.add(connector);
    }

    public ArrayList<FactoryConnector> getExitConnectors() {
        return exitConnectors;
    }

    public void addExitConnector(FactoryConnector connector) {
        this.exitConnectors.add(connector);
    }

    public BufferedImage getCurrentIcon() {
        return currentIcon;
    }

    public void setCurrentIcon(BufferedImage currentIcon) {
        this.currentIcon = currentIcon;
    }

    public double getProductionFactor() {
        return productionFactor;
    }

    public void setProductionFactor(double productionFactor) {
        this.productionFactor = productionFactor;
    }
}
