package ComponentClasses.SaleBehavior;

public interface SaleStrategy {

    public boolean canPerformSale();

}
