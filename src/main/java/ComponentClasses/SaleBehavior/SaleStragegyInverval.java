package ComponentClasses.SaleBehavior;

import java.time.Duration;
import java.time.Instant;

public class SaleStragegyInverval implements SaleStrategy {



    private Instant lastSaleTime;

    public SaleStragegyInverval(){

        lastSaleTime = Instant.now();
    }

    @Override
    public boolean canPerformSale() {

        Instant currentTime = Instant.now();
        long timeElapsed = Duration.between(lastSaleTime,currentTime).toMillis();
        if (timeElapsed > 500){
            lastSaleTime = Instant.now();
            return true;
        }
        return false;


    }
}
