package ComponentClasses.SaleBehavior;

import java.util.Random;

public class SaleStrategyRandom implements SaleStrategy {

    private int randomChance;

    public SaleStrategyRandom(int randomChance){
        this.randomChance = randomChance;
    }

    @Override
    public boolean canPerformSale() {
        Random rn = new Random();
        int randomNumber = rn.nextInt(randomChance + 1);

        return randomNumber == 1;
    }
}
