package ComponentClasses;

import DTOClasses.Connector;
import DTOClasses.FactoryConfig;

import java.util.ArrayList;

public class FactoryConnector extends Connector {

    private SimulatedLocation factory;
    private SimulatedLink link;

    private ArrayList<SimulatedComponent> heldComponents;


    public FactoryConnector(FactoryConfig.EntryOrExit eoe, SimulatedComponent.ComponentType t, int cc, SimulatedLocation loc, SimulatedLink l) {
        super(eoe, t, cc);
        factory = loc;
        link = l;
        heldComponents = new ArrayList<>();
    }

    public FactoryConnector(Connector c) {
        super(c.getFlowDirection(), c.getComponentType(), c.getComponentsRequired());
        heldComponents = new ArrayList<>();
    }

    public FactoryConnector(Connector c, SimulatedLocation loc, SimulatedLink l) {
        super(c.getFlowDirection(), c.getComponentType(), c.getComponentsRequired());
        factory = loc;
        link = l;
        heldComponents = new ArrayList<>();
    }

    public FactoryConnector(FactoryConfig.EntryOrExit eoe, SimulatedComponent.ComponentType t, int cc) {
        super(eoe, t, cc);
    }

    public void insertComponent(SimulatedComponent component){

        if (super.getFlowDirection() == FactoryConfig.EntryOrExit.EXIT){
            sendComponentToLink(component);
        }
        else if (super.getFlowDirection() == FactoryConfig.EntryOrExit.ENTRY){
            component.setDisplayed(false);
            heldComponents.add(component);
        }
    }



    public SimulatedComponent extractComponent(){
        if (getNumberOfHeldComponents() > 0){
            return getComponentFromStorage();
        }
        else {
            return null;
        }
    }

    private void sendComponentToLink(SimulatedComponent component){
        component.setDisplayed(true);
        component.setxPosition(link.getFirstFactoryConnector().getFactory().getxPosition());
        component.setyPosition(link.getFirstFactoryConnector().getFactory().getyPosition());
        link.transferComponent(component);
    }

    private SimulatedComponent getComponentFromStorage(){
        SimulatedComponent componentToExtract = heldComponents.get(0);
        heldComponents.remove(0);
        componentToExtract.setDisplayed(false);
        return componentToExtract;
    }

    public boolean containsEnoughComponents(){
        return heldComponents.size() >= super.getComponentsRequired();
    }

    public int getNumberOfHeldComponents(){
        return heldComponents.size();
    }


    public SimulatedLocation getFactory() {
        return factory;
    }

    public void setFactory(SimulatedLocation factory) {
        this.factory = factory;
    }

    public SimulatedLink getLink() {
        return link;
    }

    public void setLink(SimulatedLink link) {
        this.link = link;
    }


}
