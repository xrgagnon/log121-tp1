package ComponentClasses;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

public class SimulatedIcon extends JLabel implements Simulated  {


    private int xPosition;
    private int yPosition;
    private BufferedImage defaultImage;
    private boolean displayed = true;

    public SimulatedIcon(int x, int y, BufferedImage i) {
        super();
        xPosition = x;
        yPosition = y;
        defaultImage = i;

    }



    @Override
    public void Simulate() {
        xPosition++;
    }

    @Override
    public void Draw(Graphics g) {
        if (this.displayed)
        g.drawImage(defaultImage,xPosition,yPosition,this);
    }

    public int getxPosition() {
        return xPosition;
    }

    public void setxPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }

    public void setyPosition(int yPosition) {
        this.yPosition = yPosition;
    }

    public BufferedImage getDefaultImage() {
        return defaultImage;
    }

    public void setDefaultImage(BufferedImage defaultImage) {
        this.defaultImage = defaultImage;
    }

    public void addToPosition(int x, int y){
        xPosition += x;
        yPosition += y;
    }

    public void setDisplayed(boolean dis){
        displayed = dis;
    }

    public boolean isDisplayed() {
        return displayed;
    }
}
