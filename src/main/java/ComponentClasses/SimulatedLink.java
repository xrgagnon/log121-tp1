package ComponentClasses;

import java.awt.*;
import java.util.ArrayList;

public class SimulatedLink implements Simulated {

    private FactoryConnector firstFactoryConnector;
    private FactoryConnector secondFactoryConnector;

    private ArrayList<ComponentInTransit> componentsInTransit;

    private int initialXPosition;
    private int initialYPosition;

    private int finalXPosition;
    private int finalYPosition;

    private final int movementSpeed = 1;

     public SimulatedLink(){
        componentsInTransit = new ArrayList<>();
     }

    public void transferComponent(SimulatedComponent component){

         ComponentInTransit newComponentInTransit = new ComponentInTransit(component,movementSpeed);
         componentsInTransit.add(newComponentInTransit);
         component.setVisible(true);

    }

    @Override
    public void Simulate() {
        moveAllComponents();
    }

    @Override
    public void Draw(Graphics g) {
        int offset = 10;
        g.drawLine(initialXPosition + offset, initialYPosition + offset,finalXPosition + offset,finalYPosition + offset);
    }

    private void moveAllComponents(){
        ArrayList<ComponentInTransit> arrivedComponents = new ArrayList<>();

        componentsInTransit.forEach(componentInTransit -> {

            componentInTransit.component.move(getMovementSpeed());

            if (hasArrived(componentInTransit)){
                secondFactoryConnector.insertComponent(componentInTransit.component);
                arrivedComponents.add(componentInTransit);
            }

        });

        componentsInTransit.removeAll(arrivedComponents);
    }

    private boolean hasArrived(ComponentInTransit componentInTransit){
        return componentInTransit.component.getxPosition() == finalXPosition && componentInTransit.component.getyPosition() == finalYPosition;
    }

    private void updateCoordinates(){
        try {
            if (firstFactoryConnector != null){
                initialXPosition = firstFactoryConnector.getFactory().getxPosition();
                initialYPosition = firstFactoryConnector.getFactory().getyPosition();
            }
            if (secondFactoryConnector != null){
                finalXPosition = secondFactoryConnector.getFactory().getxPosition();
                finalYPosition = secondFactoryConnector.getFactory().getyPosition();
            }


        }
        catch(Exception e){
            System.out.println("Error Updating Coordinates"+e.getMessage());
        }
    }

    private int[] getMovementSpeed(){
         int[] movementSpeed = {1,1};


         if (initialXPosition > finalXPosition){
             movementSpeed[0] = -1;
         }
         else if (initialXPosition == finalXPosition){
             movementSpeed[0] = 0;
         }

         if (initialYPosition > finalYPosition){
             movementSpeed[1] = -1;
         }
         else if (initialYPosition == finalYPosition){
             movementSpeed[1] = 0;
         }

         return movementSpeed;

    }

    public FactoryConnector getFirstFactoryConnector() {
        return firstFactoryConnector;
    }

    public void setFirstFactoryConnector(FactoryConnector firstFactoryConnector) {
        this.firstFactoryConnector = firstFactoryConnector;
        updateCoordinates();
    }

    public FactoryConnector getSecondFactoryConnector() {
        return secondFactoryConnector;
    }

    public void setSecondFactoryConnector(FactoryConnector secondFactoryConnector) {
        this.secondFactoryConnector = secondFactoryConnector;
        updateCoordinates();
    }

    public int getInitialXPosition() {
        return initialXPosition;
    }

    public void setInitialXPosition(int initialXPosition) {
        this.initialXPosition = initialXPosition;
    }

    public int getInitialYPosition() {
        return initialYPosition;
    }

    public void setInitialYPosition(int initialYPosition) {
        this.initialYPosition = initialYPosition;
    }

    public int getFinalXPosition() {
        return finalXPosition;
    }

    public void setFinalXPosition(int finalXPosition) {
        this.finalXPosition = finalXPosition;
    }

    public int getFinalYPosition() {
        return finalYPosition;
    }

    public void setFinalYPosition(int finalYPosition) {
        this.finalYPosition = finalYPosition;
    }

    private class ComponentInTransit{

     private SimulatedComponent component;
     private int movementProgress = 0;
     private int movementSpeed;

        public ComponentInTransit(SimulatedComponent component, int movementSpeed) {
            this.component = component;
            this.movementSpeed = movementSpeed;
        }

        public SimulatedComponent getComponent() {
            return component;
        }

        public void setComponent(SimulatedComponent component) {
            this.component = component;
        }

        public int getMovementProgress() {
            return movementProgress;
        }

        public void setMovementProgress(int movementProgress) {
            this.movementProgress = movementProgress;
        }

        public int getMovementSpeed() {
            return movementSpeed;
        }

        public void setMovementSpeed(int movementSpeed) {
            this.movementSpeed = movementSpeed;
        }
    }
}
