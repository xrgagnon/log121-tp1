package ComponentClasses;

import ComponentClasses.SimulatedComponent.ComponentType;
import IOClasses.IconIO;
import SimulationCore.PanneauPrincipal;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class ComponentManager {

    private static BufferedImage metalIcon;
    private static BufferedImage engineIcon;
    private static BufferedImage wingIcon;
    private static BufferedImage planeIcon;

    private static final String metalIconFilePath = "src/ressources/metal.png";
    private static final String wingIconFilePath = "src/ressources/aile.png";
    private static final String engineIconFilePath = "src/ressources/moteur.png";
    private static final String planeIconFilePath = "src/ressources/avion.png";

    private static PanneauPrincipal renderer;

    //Method must be executed as part of loading the simulation
    public static void initializeFactory(PanneauPrincipal graphicPanel){

        renderer = graphicPanel;

        IconIO iconIO = new IconIO();

        metalIcon = iconIO.loadImage(metalIconFilePath);
        engineIcon = iconIO.loadImage(engineIconFilePath);
        wingIcon = iconIO.loadImage(wingIconFilePath);
        planeIcon = iconIO.loadImage(planeIconFilePath);

    }

    public static SimulatedComponent createComponent(ComponentType type){
        SimulatedComponent component;
        switch (type){
            case METAL:
                component = new SimulatedComponent(metalIcon,ComponentType.METAL);
                break;
            case WING:
                component = new SimulatedComponent(wingIcon,ComponentType.WING);
                break;
            case ENGINE:
                component = new SimulatedComponent(engineIcon,ComponentType.ENGINE);
                break;
            case PLANE:
                component = new SimulatedComponent(planeIcon,ComponentType.PLANE);
                break;
            default:
                component = null;
        }
        if (component != null){
            renderer.addSimulatedObject(component);
        }
        SimulationStatistics.notifyNewComponentProduced(component);
        return component;
    }

    public static void removeComponent(SimulatedComponent component){
        renderer.removeSimulatedObject(component);
    }

    public static void removeComponents(ArrayList<SimulatedComponent> components){

        components.forEach(simulatedComponent -> {
            renderer.removeSimulatedObject(simulatedComponent);
        });

    }

}
