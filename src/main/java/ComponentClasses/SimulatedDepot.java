package ComponentClasses;

import ComponentClasses.SaleBehavior.SaleStrategy;
import DTOClasses.FactoryConfig;


import java.awt.image.BufferedImage;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;

public class SimulatedDepot extends SimulatedLocation {

    private int storageCapacity;
    private ArrayList<SimulatedComponent> storedComponents;

    private PropertyChangeSupport observerSupport;

    private SaleStrategy saleStrategy;

    public SimulatedDepot(int x, int y, BufferedImage[] si, int id, FactoryConfig c, FactoryConfig.FactoryType type) {
        super(x, y, si, id, c, type);

        storedComponents = new ArrayList<>();
        storageCapacity = 0;
        observerSupport = new PropertyChangeSupport(this);

        super.getEntryConnectors().forEach(factoryConnector -> {
            storageCapacity += factoryConnector.getComponentsRequired();
        });

        saleStrategy = SimulationStatistics.getCurrentSaleStrategy();
    }

    @Override
    public void Simulate(){
        int oldStorageLevel = storedComponents.size();

        saleActions();

        storeComponents();

        notifyIfStorageLevelChangedFrom(oldStorageLevel);

        updateIcon();

    }

    private void checkForChangeInSaleStrategy(){
        if (SimulationStatistics.hasStrategyChanged()){
            saleStrategy = SimulationStatistics.getCurrentSaleStrategy();
            SimulationStatistics.saleStrategyUpdateConfirmed();
        }
    }

    private void notifyIfStorageLevelChangedFrom(int oldStorageLevel){

        if (oldStorageLevel != storedComponents.size()){
            double oldStoragePercentage =oldStorageLevel/(double)storageCapacity;
            double storagePercentage = storedComponents.size()/(double)storageCapacity;
            notifyFactoriesOfStorageLevel(oldStoragePercentage,storagePercentage);
        }
    }

    private void storeComponents(){
        super.getEntryConnectors().forEach(connector -> {

            if (connector.getNumberOfHeldComponents() > 0){
                SimulatedComponent extractedComponent = connector.extractComponent();
                storedComponents.add(extractedComponent);
            }
        });
    }

    private void saleActions(){
        checkForChangeInSaleStrategy();
        if (saleStrategy.canPerformSale() && storedComponents.size() > 0){
            doSale();
        }
    }

    private void doSale(){
        SimulationStatistics.planeSold();
        if (storedComponents.size() > 0){
            storedComponents.remove(0);
            System.out.println("Plane Sold!");
        }

    }

    private void updateIcon(){

        double usedStorage = (double)storedComponents.size()/(double)storageCapacity;

        if (usedStorage < 0.25){
            super.setCurrentIcon(super.getStateImages()[0]);
        }
        else if (usedStorage < 0.50){
            super.setCurrentIcon(super.getStateImages()[1]);
        }
        else if (usedStorage < 0.75){
            super.setCurrentIcon(super.getStateImages()[2]);
        }
        else if (usedStorage >= 0.75) {
            super.setCurrentIcon(super.getStateImages()[3]);
        }
        else {
            super.setCurrentIcon(super.getStateImages()[0]);
        }

    }

    private void notifyFactoriesOfStorageLevel(double oldStoragePercentage, double newStoragePercentage){
        observerSupport.firePropertyChange("Storage Level",oldStoragePercentage,newStoragePercentage);
    }


    public void addListener(PropertyChangeListener listener){
        observerSupport.addPropertyChangeListener(listener);
    }

    public void removeListener(PropertyChangeListener listener){
        observerSupport.removePropertyChangeListener(listener);
    }

    @Override
    public void addEntryConnector(FactoryConnector connector) {
        storageCapacity += connector.getComponentsRequired();
        super.addEntryConnector(connector);
    }

    public SaleStrategy getSaleStrategy() {
        return saleStrategy;
    }

    public void setSaleStrategy(SaleStrategy saleStrategy) {
        this.saleStrategy = saleStrategy;
    }
}
