package ComponentClasses;

import DTOClasses.FactoryConfig;

import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Observable;
import java.util.Observer;

public class SimulatedFactory extends SimulatedLocation implements PropertyChangeListener {


    public SimulatedFactory(int x, int y, BufferedImage[] si, int id, FactoryConfig c, FactoryConfig.FactoryType type) {
        super(x, y, si, id, c, type);
    }


    @Override()
    public void Simulate(){
        super.Simulate();
    }


    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName().equals("Storage Level")){
            adjustProduction((Double) evt.getNewValue());
        }
    }

    private void adjustProduction(double storagePercentage){
        if (storagePercentage < 0.5){
            super.setProductionFactor(1);
        }
        else if (storagePercentage < 0.75){
            super.setProductionFactor(0.5);
        }
        else {
            super.setProductionFactor(0.1);
        }
    }
}
