package ComponentClasses;

import java.awt.*;

public interface Simulated {

    public void Simulate();
    public void Draw(Graphics g);
}
