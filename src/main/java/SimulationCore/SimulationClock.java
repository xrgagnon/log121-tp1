package SimulationCore;

public class SimulationClock {


    private static int simulationCycleLength = 10;

    public static void updateSimulationCycleLength(int newCycleLength){
        simulationCycleLength = newCycleLength;
    }

    public static int getElapsedTime(){

        return simulationCycleLength;

    }

}
