package SimulationCore;



import ComponentClasses.Simulated;
import ComponentClasses.SimulatedIcon;
import ComponentClasses.SimulatedLink;
import ComponentClasses.ComponentManager;
import IOClasses.SimulationInitializer;
import IOClasses.IconIO;

import java.awt.*;
import java.util.ArrayList;
import java.util.LinkedList;

import javax.swing.JPanel;


public class PanneauPrincipal extends JPanel {

	private static final long serialVersionUID = 1L;


	private LinkedList<Simulated> simulationObjects = new LinkedList<Simulated>();
	private LinkedList<Simulated> pendingObjects = new LinkedList<Simulated>();
	private LinkedList<Simulated> pendingRemovalObjects = new LinkedList<Simulated>();


	public PanneauPrincipal() {
		IconIO iconIO = new IconIO();
		SimulationInitializer simulationInitializer = new SimulationInitializer();

		ComponentManager.initializeFactory(this);


		ArrayList<Simulated> objectsToSimulate = new ArrayList<>();

		objectsToSimulate = simulationInitializer.getSimulationObjects();

		simulationObjects.addAll(objectsToSimulate);

		simulationObjects.remove(pendingRemovalObjects);
		pendingRemovalObjects.clear();
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);

		simulationObjects.forEach(Simulated::Simulate);
		simulationObjects.addAll(pendingObjects);
		pendingObjects.clear();

		g.setColor(Color.RED);
		for(Simulated sim : simulationObjects){
			if (sim instanceof SimulatedLink)
				sim.Draw(g);
		}

		g.setColor(Color.BLUE);
		for(Simulated sim : simulationObjects){
			if (sim instanceof SimulatedIcon)
				sim.Draw(g);
		}


	}

	public void addSimulatedObject(Simulated obj){
		pendingObjects.add(obj);
	}

	public void removeSimulatedObject(Simulated obj){
		pendingRemovalObjects.remove(obj);
	}

}