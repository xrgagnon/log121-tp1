package DTOClasses;


import java.util.ArrayList;

public class FactoryConfig {
    public enum FactoryType {ENGINE_FACTORY,ASSEMBLY_FACTORY,WING_FACTORY,MATTER_FACTORY,DEPOT}
    public enum EntryOrExit {ENTRY, EXIT}

    private FactoryType type;
    private String[] imagePaths;
    private int productionInterval;
    private ArrayList<Connector> entryConnectors;
    private ArrayList<Connector> exitConnectors;

    @Override
    public String toString(){
        return type.toString() + " -- " +productionInterval;
    }

    public FactoryConfig(FactoryType type, String[] imagePaths, int productionInterval, ArrayList<Connector> entryConnectors, ArrayList<Connector> exitConnectors) {
        this.type = type;
        this.imagePaths = imagePaths;
        this.productionInterval = productionInterval;
        this.entryConnectors = entryConnectors;
        this.exitConnectors = exitConnectors;
    }

    public FactoryType getType() {
        return type;
    }

    public void setType(FactoryType type) {
        this.type = type;
    }

    public String[] getImagePaths() {
        return imagePaths;
    }

    public void setImagePaths(String[] imagePaths) {
        this.imagePaths = imagePaths;
    }

    public int getProductionInterval() {
        return productionInterval;
    }

    public void setProductionInterval(int productionInterval) {
        this.productionInterval = productionInterval;
    }

    public ArrayList<Connector> getEntryConnectors() {
        return entryConnectors;
    }

    public void setEntryConnectors(ArrayList<Connector> entryConnectors) {
        this.entryConnectors = entryConnectors;
    }

    public ArrayList<Connector> getExitConnectors() {
        return exitConnectors;
    }

    public void setExitConnectors(ArrayList<Connector> exitConnectors) {
        this.exitConnectors = exitConnectors;
    }
}
