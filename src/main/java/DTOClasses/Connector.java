package DTOClasses;

import ComponentClasses.SimulatedComponent;

public class Connector {



    private FactoryConfig.EntryOrExit flowDirection;
    private SimulatedComponent.ComponentType componentType;
    private int componentsRequired;

    public Connector(FactoryConfig.EntryOrExit eoe, SimulatedComponent.ComponentType t, int cc){
        flowDirection = eoe;
        componentType = t;
        componentsRequired = cc;
    }

    public FactoryConfig.EntryOrExit getFlowDirection() {
        return flowDirection;
    }

    public void setFlowDirection(FactoryConfig.EntryOrExit flowDirection) {
        this.flowDirection = flowDirection;
    }

    public SimulatedComponent.ComponentType getComponentType() {
        return componentType;
    }

    public void setComponentType(SimulatedComponent.ComponentType componentType) {
        this.componentType = componentType;
    }

    public int getComponentsRequired() {
        return componentsRequired;
    }

    public void setComponentsRequired(int componentsRequired) {
        this.componentsRequired = componentsRequired;
    }
}
